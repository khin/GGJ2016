﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IObserver {

    void OnNotification(object sender, string message, params object[] args);

}

public static class MessageSystem
{
    private static Dictionary<object, IObserver[]> listeners = new Dictionary<object, IObserver[]>();

    public static void Notify(this object sender, string message, params object[] args)
    {
        if (listeners.ContainsKey(sender))
            foreach (IObserver observer in listeners[sender])
                observer.OnNotification(sender, message, args);
    }

    public static void RegisterTo(this IObserver observer, object subject)
    {
        List<IObserver> list;
        if (!listeners.ContainsKey(subject))
            listeners.Add(subject, new IObserver[] { observer });
        else
        {
            list = listeners[subject].ToList();
            if (!list.Contains(observer))
                list.Add(observer);
            listeners[subject] = list.ToArray();
        }
    }
}