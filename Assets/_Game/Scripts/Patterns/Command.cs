﻿using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Reflection;

public abstract class Command {

    // Default Constructor
    public Command() { }

    // Required
    //public abstract void Execute(GameActor actor);
    public abstract void Execute(GameActor actor, params object[] args);

    // Optional
    public virtual void Undo() { }
    public virtual void Redo() { }

    // Base-class Implemented

}