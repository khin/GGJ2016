﻿using UnityEngine;
using System.Collections;

public class Messages {
	#region Health

	public const string PlayerHealthChanged	= "player.health";
	public const string PlayerDeath			= "player.death";

	#endregion


	#region Player Element

	public const string ElementChanged		= "player.element.changed";

	#endregion


	#region Magic

	public const string ArmorActivated		= "magic.armor.activated";
	public const string ArmorDeactivated	= "magic.armor.deactivated";
	public const string MagicAttack			= "magic.attack";
	public const string MagicHit			= "magic.hit";

	#endregion
}
