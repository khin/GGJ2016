﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InputHandler))]
public class Player : GameActor {

	private Rigidbody2D rb;
	private Caster caster;
	private Vector2 addedForce;
	private bool isAttacking;

    private Animator anim { get { return GetComponent<Animator>(); } }

	private Element element;

	private GameObject currentSpell;

	public AudioClip changeElementSound;

	[SerializeField]
	private Transform CastPoint;

	public float moveForce;

    public const int MAX_PLAYERS = 2;
    public static Player[] refs = new Player[MAX_PLAYERS];

    // Use this for initialization
    void Start () {
		rb = GetComponent<Rigidbody2D>();
		addedForce = new Vector2 (0, moveForce);
		element = GetComponent<Element>();
		caster = GetComponent<Caster>();
		isAttacking = false;

        refs[GetComponent<InputHandler>().joystickID - 1] = this;
}

	public void setVelocity(int velocity) {
		rb.velocity = new Vector2 (0, velocity);
	}
	
	// Update is called once per frame
	void Update () {
        anim.SetFloat("speed_y", rb.velocity.y);
		//setVelocity (0);
		if(isAttacking) {
			checkSpellEnd();
		}
	}

	public void OnMoveUp(){
		rb.velocity = addedForce;
	}

	public void OnMoveDown() {
		rb.velocity = -addedForce;
	}

	public void ChangeElement(ElementType type) {
		element.ChangeElement(type);
		ElementAura aura = GetComponentInChildren<ElementAura>();
		aura.SetAura(type);
		if(type != ElementType.None)
			GetComponent<AudioSource>().PlayOneShot(changeElementSound);
	}

	public ElementType GetElement() {
		return element.GetElementType();
	}

	public Vector2 GetCastPoint() {
		return new Vector2(CastPoint.position.x, CastPoint.position.y);
	}

	public Vector2 GetForward() {
		InputHandler input = GetComponent<InputHandler>();

		if (input.joystickID == 1)
			return Vector2.right;
		else
			return Vector2.left;
	}

	public void castSpell(bool isAttack) {
		if (isAttack && GetElement() == ElementType.None)
			return;

		if (!isAttacking) {
			isAttacking = true;
			currentSpell = caster.CastSpell(isAttack, GetElement(), GetCastPoint(), (GetComponent<Rigidbody2D>().velocity.normalized + GetForward()).normalized, gameObject.layer);
			ChangeElement(ElementType.None);
            anim.SetTrigger("casting");
		}
	}

	private void checkSpellEnd() {
		if (currentSpell != null) {
			MagicAttack spellCasted;
			spellCasted = currentSpell.GetComponent<MagicAttack>();
			
			if (spellCasted != null) {
				isAttacking = spellCasted.isValid;
				if (!spellCasted.isValid && !spellCasted.isReflecting) {
                    //Destroy(currentSpell);
                    spellCasted.doneReflecting ();
				}
			}
			
		}
		else {
			isAttacking = caster.IsArmorActive();
		}
	}
}
