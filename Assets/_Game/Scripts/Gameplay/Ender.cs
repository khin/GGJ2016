﻿using UnityEngine;
using System.Collections;

public class Ender : MonoBehaviour, IObserver {
	public bool ended;

	public GameObject endWindow;

	// Use this for initialization
	void Start () {
		Health[] healths = FindObjectsOfType<Health>();
		this.RegisterTo (healths[0]);
		this.RegisterTo (healths[1]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnNotification(object sender, string message, params object[] args) {
		if (message == Messages.PlayerDeath) {
			ended = true;
			endWindow.SetActive (true);
		}
	}

}
