﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum SpellMoveType
{
    Empty,
    Forward,
    Backward
}

public class SpellCombo : Command {

    public const int COMBO_SIZE = 2;

    private int currentMove = 0;
    private SpellMoveType[] moveSequence = new SpellMoveType[COMBO_SIZE];

    public SpellCombo()
    {
        ResetCombo();
    }

    // args[0] = Direction of player press of spell
    public override void Execute(GameActor actor, params object[] args)
    {
		if (args[0] != null) {
			float dir = (float)args[0];

			moveSequence[currentMove] = (dir > 0f) ? SpellMoveType.Forward : SpellMoveType.Backward;

			if (currentMove == (COMBO_SIZE - 1)) {
				((Player)actor).ChangeElement(GetElement(moveSequence));
				ResetCombo();
			}
			else
				currentMove += 1;
		}

		if (args.Length == 2) {
			ElementType type = (ElementType)args[1];

			((Player)actor).ChangeElement(type);
			ResetCombo();
		}
	}

    private SpellMoveType[] FireSequence    = new SpellMoveType[] { SpellMoveType.Forward, SpellMoveType.Forward };
    private SpellMoveType[] WaterSequence   = new SpellMoveType[] { SpellMoveType.Backward, SpellMoveType.Backward };
    private SpellMoveType[] WindSequence    = new SpellMoveType[] { SpellMoveType.Backward, SpellMoveType.Forward };
    private SpellMoveType[] EarthSequence   = new SpellMoveType[] { SpellMoveType.Forward, SpellMoveType.Backward };

    public ElementType GetElement(SpellMoveType[] moveSequence)
    {
        if (ComboEqualTo(moveSequence, FireSequence))
            return ElementType.Fire;
        else if (ComboEqualTo(moveSequence, WaterSequence))
            return ElementType.Water;
        else if (ComboEqualTo(moveSequence, WindSequence))
            return ElementType.Wind;
        else if (ComboEqualTo(moveSequence, EarthSequence))
            return ElementType.Earth;
        else
            return ElementType.None;

    }

    public bool ComboEqualTo(SpellMoveType[] combo, SpellMoveType[] elementSequence)
    {
            for (int i = 0; i < COMBO_SIZE; i++)
            {
                if (combo[i] != elementSequence[i])
                    return false;
            }
            return true;

    }

    public void ResetCombo()
    {
        for (int i = 0; i < COMBO_SIZE; i++)
            moveSequence[i] = SpellMoveType.Empty;
        currentMove = 0;
    }
}

public class CastAttack : Command
{
    public override void Execute(GameActor actor, params object[] args)
    {
		Player player = (Player)actor;
		player.castSpell(true);
    }
}

public class CastDefense : Command
{
    public override void Execute(GameActor actor, params object[] args)
    {
		Player player = (Player)actor;
		player.castSpell(false);        
    }
}