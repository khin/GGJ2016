﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public abstract class GameActor : MonoBehaviour
{

    protected virtual void Awake()
    {
        this.AttachReferences();
    }

    protected virtual void AttachReferences()
    {

    }
}