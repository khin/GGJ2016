﻿using UnityEngine;
using System.Collections;

public class Movement : Command {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	public override void Execute(GameActor player, params object[] args) {
		Player p = (Player)player;
		float axis = (float)args [0];

		if (axis > 0)
			p.OnMoveUp();
		else if (axis < 0)
			p.OnMoveDown();
		else
			p.setVelocity(0);
	}
}
