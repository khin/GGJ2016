﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameActor))]
public class InputHandler : MonoBehaviour {

    public enum ScreenPosition
    {
        LeftScreenSide,
        RightScreenSide
    }

    [Range(1, 2)]
    public int joystickID = 1;

    public ScreenPosition playerPosition;

	[SerializeField]
    private string movementAxis = "Vertical";
    [SerializeField]
    private string spellAxis = "Horizontal";
	private float pressedTime = 0;
	public float intervalTime;

	private GameActor player
    {
        get { return GetComponent<GameActor>(); }
    }

	Movement moveCommand = new Movement();
    SpellCombo spellCommand = new SpellCombo();

	// Update is called once per frame
	void Update () {

		moveCommand.Execute (player, Input.GetAxis (movementAxis + joystickID));

        // Spell Check
        if ( Input.GetButtonDown(spellAxis + joystickID) )
        {
            float direction = (playerPosition == ScreenPosition.RightScreenSide) ? -1f : 1f;
            spellCommand.Execute(player, Input.GetAxis(spellAxis + joystickID) * direction);
        }
		else if (Input.GetButtonDown("ElementFire" + joystickID)) {
			spellCommand.Execute(player, null, ElementType.Fire);
		}
		else if (Input.GetButtonDown("ElementWater" + joystickID)) {
			spellCommand.Execute(player, null, ElementType.Water);
		}
		else if (Input.GetButtonDown("ElementEarth" + joystickID)) {
			spellCommand.Execute(player, null, ElementType.Earth);
		}
		else if (Input.GetButtonDown("ElementWind" + joystickID)) {
			spellCommand.Execute(player, null, ElementType.Wind);
		}
		else if (Input.GetButtonDown("ElementReset" + joystickID)) {
			spellCommand.Execute(player, null, ElementType.None);
		}

		if (Input.GetButtonDown("CastAttack" + joystickID) || Input.GetAxisRaw("CastAttack" + joystickID) > 0)
        {
            CastAttack attackSpell = new CastAttack();
            attackSpell.Execute(player);
			spellCommand.ResetCombo();
		}

        if(Input.GetButtonDown("CastDefense" + joystickID) || Input.GetAxisRaw("CastDefense" + joystickID) > 0)
        {
            CastDefense defenseSpell = new CastDefense();
            defenseSpell.Execute(player);
			spellCommand.ResetCombo();
        }
	}
}
