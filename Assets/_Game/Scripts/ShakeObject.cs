﻿using UnityEngine;
using System.Collections;

public class ShakeObject : MonoBehaviour {
	
	public void Shake(float magnitude, float duration) {
		StartCoroutine(ShakeCoroutine(magnitude, duration));
	}

	public IEnumerator ShakeCoroutine(float magnitude, float duration) {
		float time = 0f;

		Vector3 startPos = transform.position;

		while (time < duration) {
			time += Time.deltaTime;

			transform.position = startPos + Vector3.right * Random.Range(-magnitude, magnitude) + Vector3.up * Random.Range(-magnitude, magnitude);

			yield return null;
		}

		transform.position = startPos;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
