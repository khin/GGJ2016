﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MagicArmor))]
public class Health : MonoBehaviour {
	[SerializeField]
	private float _HP	= 100.0f;
    public float HPvalue
    {
        get { return _HP; }
    }

	public AudioClip hurtSound;

	private bool _dead	= false;
	
	public bool isDead {
		get { return _dead; }
	}

	public void Damage(float damage) {
		if (isDead) return;

		_HP -= damage;

		if (_HP <= 0.0f) {
			_HP = 0.0f;
			Death();
		}

		this.Notify(Messages.PlayerHealthChanged, new object[] { _HP });
	}

	private void Death() {
		_dead = true;

		this.Notify(Messages.PlayerDeath, null);
	}

	public void InstantKill() {
		_HP = 0.0f;
		Death();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
