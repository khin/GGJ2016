﻿using UnityEngine;
using System.Collections;

public enum ElementType {
	None = 0,
	Fire = 1,
	Water = 2,
	Earth = 3,
	Wind = 4
}

public class Element : MonoBehaviour {
	[SerializeField]
	private ElementType _type = ElementType.None;

	public void ChangeElement(ElementType type) {
		_type = type;

		this.Notify(Messages.ElementChanged, new object[] { type });
	}

	public ElementType GetElementType() { return _type; }

	public ElementType ConsumeElement() {
		ElementType type = _type;

		ResetElement();

		return type;
	}

	public void ResetElement() {
		ChangeElement(ElementType.None);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
