﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Element))]
public class MagicArmor : MonoBehaviour {
	[SerializeField, Tooltip("Duration of defense stance, in seconds")]
	private float _duration = 1.0f;

	[SerializeField, Tooltip("Time to expire armor")]
	private float _timer = 0.0f;

	private bool _isActive = false;

	public bool isActive { get { return _isActive; } }

	[SerializeField]
	private GameObject NeutralArmor;
	[SerializeField]
	private GameObject FireArmor;
	[SerializeField]
	private GameObject WaterArmor;
	[SerializeField]
	private GameObject EarthArmor;
	[SerializeField]
	private GameObject WindArmor;

	public void Activate(ElementType type = ElementType.None) {
		if (isActive) return;

		_isActive = true;
		switch (type) {
			case ElementType.None:	NeutralArmor.SetActive(true);	break;
			case ElementType.Fire:	FireArmor.SetActive(true);		break;
			case ElementType.Water:	WaterArmor.SetActive(true);		break;
			case ElementType.Earth:	EarthArmor.SetActive(true);		break;
			case ElementType.Wind:	WindArmor.SetActive(true);		break;
			default: break;
		}

		_timer = _duration;

		this.Notify(Messages.ArmorActivated, null);
	}

	private void Deactivate() {
		_isActive = false;
		_timer = 0.0f;

		NeutralArmor.SetActive(false);
		FireArmor.SetActive(false);
		WaterArmor.SetActive(false);
		EarthArmor.SetActive(false);
		WindArmor.SetActive(false);

		this.Notify(Messages.ArmorDeactivated, null);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isActive) {
			UpdateTimer();
		}
	}

	private void UpdateTimer() {
		_timer -= Time.deltaTime;

		if (_timer <= 0.0f)
			Deactivate();
	}
}
