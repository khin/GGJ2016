﻿using UnityEngine;
using System.Collections;

public class Caster : MonoBehaviour {

	[SerializeField]
	private GameObject FireAttack;
	[SerializeField]
	private GameObject WaterAttack;
	[SerializeField]
	private GameObject EarthAttack;
	[SerializeField]
	private GameObject WindAttack;

	private MagicArmor armor;

	[SerializeField]
	private float _force = 10.0f;
	[SerializeField]
	private ForceMode2D _forceMode = ForceMode2D.Force;

	public GameObject CastSpell(bool isAttack, ElementType type, Vector2 source, Vector2 direction, int layer) {
		GameObject spell = null;

		if (isAttack) {
			switch (type) {
			case ElementType.Fire:
				spell = (GameObject)Instantiate (FireAttack, new Vector3 (source.x, source.y, 0), Quaternion.identity);
				break;
			case ElementType.Water:
				spell = (GameObject)Instantiate (WaterAttack, new Vector3 (source.x, source.y, 0), Quaternion.identity);
				break;
			case ElementType.Earth:
				spell = (GameObject)Instantiate (EarthAttack, new Vector3 (source.x, source.y, 0), Quaternion.identity);
				break;
			case ElementType.Wind:
				spell = (GameObject)Instantiate (WindAttack, new Vector3 (source.x, source.y, 0), Quaternion.identity);
				break;
			default:
				break;
			}
		} else {
			armor.Activate(type);
		}			

		if (spell != null) {
			spell.layer = layer;
			spell.GetComponent<Rigidbody2D>().AddForce(direction.normalized * _force, _forceMode);
		}
		return spell;
	}

	public bool IsArmorActive() {
		return armor.isActive;
	}

	// Use this for initialization
	void Start () {
		armor = GetComponent<MagicArmor>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
