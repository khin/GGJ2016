﻿using UnityEngine;
using System.Collections;

public class MagicAttack : MonoBehaviour
{

    private static MagicAttack instance
    {
        get { return FindObjectOfType<MagicAttack>(); }
    }

    [SerializeField]
    private float _damage = 5.0f;
    [SerializeField]
    private float _damageBoost = 1.5f;
    [SerializeField, Tooltip("Speed multiplier when reflected")]
    private float _speedBoost = 1.2f;
    [SerializeField]
    private float minimalVelocity = 1f;

    [SerializeField]
    private ElementType _type = ElementType.None;

	public AudioClip absorbSound;

	public AudioClip reflectSound;

	private SpriteRenderer sprite;

    private Rigidbody2D rb;

    private Animator anim;

    private bool _isValid = true;

    private bool _isReflecting = false;

    public float Damage { get { return _damage; } }

    public bool isValid { get { return _isValid; } }

    public bool isReflecting { get { return _isReflecting; } }

    public void OnHitTarget(Health target)
    {
        target.Damage(Damage);
    }

    public void SetDamage(float damage)
    {
        _damage = damage;
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
		sprite = GetComponent<SpriteRenderer>();
    }
    public void doneReflecting()
    {
        _isReflecting = false;
    }

    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector3.Lerp(rb.velocity, (rb.velocity.x * Vector2.right).normalized * rb.velocity.magnitude, 0.025f);
        if (rb.velocity.magnitude < minimalVelocity)
            rb.drag = 0f;

		if (rb.velocity.x < 0)
			sprite.flipX = true;
		else if (rb.velocity.x > 0)
			sprite.flipX = false;

		//transform.forward = rb.velocity.normalized;

		if (!GetComponent<Renderer>().isVisible)
			Destroy(this.gameObject);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        LayerMask layer = coll.collider.gameObject.layer;

		if (LayerMask.LayerToName(layer).Contains("Map Bounds"))
			coll.collider.GetComponent<AudioSource>().Play();

        if (LayerMask.LayerToName(layer).Contains("Player"))
        {
			if (coll.collider.gameObject.CompareTag("Totem")) {
				coll.collider.gameObject.GetComponent<ShakeObject>().Shake(0.08f, 2f);
				coll.collider.GetComponent<AudioSource>().Play();
			}

            _isValid = false;
            Health health;

            if (layer == LayerMask.NameToLayer("Player1"))
                health = Player.refs[0].GetComponent<Health>();
            else
                health = Player.refs[1].GetComponent<Health>();

            if (health)
            {
                health.Damage(Damage);
				health.GetComponent<AudioSource>().PlayOneShot(health.hurtSound);
                anim.SetTrigger("Hit");
				GetComponent<Collider2D>().enabled = false;
				rb.velocity = Vector2.zero;
				ActionHelper.PerformDelayedAction(2f, () => { Destroy(this.gameObject); });
            }
        }
		else if (LayerMask.LayerToName(layer).Contains("Armor")) {
			ElementType armorType = coll.collider.gameObject.GetComponent<MagicArmorType>().type;
			GameObject armor = coll.collider.gameObject;

			if (armorType == _type) {
				coll.collider.GetComponentInParent<AudioSource>().PlayOneShot(reflectSound);
				ReflectAttack(coll.collider.transform.position);
				anim.SetTrigger("Reflect");
			}
			else if (armorType == ElementType.None) {
				AbsorbAttack();
				armor.transform.parent.GetComponent<AudioSource>().PlayOneShot(absorbSound);
				GetComponent<Collider2D>().enabled = false;
				rb.velocity.Set(0, 0);
				anim.SetTrigger("Absorb");
			}
			else {
				armor.transform.parent.GetComponent<Health>().Damage(Damage * _damageBoost);
				GetComponent<Collider2D>().enabled = false;
				anim.SetTrigger("Hit");
				rb.velocity = Vector2.zero;
				ActionHelper.PerformDelayedAction(2f, () => { Destroy(this.gameObject); });
			}
		}
    }

    public void AbsorbAttack() {
		rb.velocity = Vector2.zero;
        ActionHelper.PerformDelayedAction(2f, () => { Destroy(this.gameObject); });
    }

	public void ReflectAttack(Vector2 targetPosition) {
		Vector2 reflected = (((Vector2)(transform.position)) - targetPosition).normalized;//Vector2.Reflect(rb.velocity, normal.normalized);
		_isReflecting = true;
		gameObject.layer = (gameObject.layer == LayerMask.NameToLayer("Player1")) ? LayerMask.NameToLayer("Player2") : LayerMask.NameToLayer("Player1");
		_isValid = false;

		float magnitude = rb.velocity.magnitude;
		rb.velocity = Vector2.zero;
		rb.AddForce(reflected * magnitude * _speedBoost, ForceMode2D.Impulse);



		Time.timeScale = 0.2f;
		ScreenOverlay.SetColor(Color.magenta);
		ScreenOverlay.SetAlpha(0.2f);
		ActionHelper.PerformDelayedAction(0.15f, () => { Time.timeScale = 1f; ScreenOverlay.SetAlpha(0f); });	
    }
}
