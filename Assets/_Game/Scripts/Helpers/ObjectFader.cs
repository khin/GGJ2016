﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ::=====================================::
/// :: Object Fader                        ::
/// ::=====================================::
/// :: Description: Color fading manager   ::
/// ::              for arbitrary Unity    ::
/// ::              GameObject and object  ::
/// ::              hierarchies and lists. ::
/// ::                                     ::
/// :: Author: Leonardo Tagliaro           ::
/// ::=====================================::
/// </summary>

public static class ObjectFader
{

    private static Hashtable hierarchyLists = new Hashtable();
    private static Dictionary<GameObject, Coroutine> fadeRoutines = new Dictionary<GameObject, Coroutine>();
    private static Dictionary<Coroutine, OnObjectEndFade> endFadeFunctions = new Dictionary<Coroutine, OnObjectEndFade>();

    public delegate void OnObjectEndFade();
    private delegate void OnHierarchyEndFade(Transform root);

    /// <summary>
    /// Fades a single object alpha value to a target in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading.
    /// </summary>
    public static void FadeAlpha(GameObject obj, float alpha, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        Color targetColor = ColorManager.GetObjectColor(obj, colorProperty);
        targetColor.a = alpha;
        FadeColor(obj, targetColor, duration, colorProperty, endFadeFunction);
    }

    /// <summary>
    /// Fades the alpha component of a list of objects to a target color in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading all objects.
    /// </summary>
    public static void FadeAlphaList(List<GameObject> objects, float alpha, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        int key = LowestAvailableKey();
        hierarchyLists.Add(key, new List<Transform>());

        foreach (GameObject obj in objects)
        {
            if (obj.GetComponent<Renderer>() != null && obj.activeInHierarchy)
                if (obj.GetComponent<Renderer>().material.HasProperty(colorProperty))
                    ((List<Transform>)hierarchyLists[key]).Add(obj.transform);

            FadeAlpha(obj, alpha, duration, colorProperty, () => ObjectsFadeEndSync(endFadeFunction, key, obj.transform));
        }
    }

    /// <summary>
    /// Fades an hierarchy of objects alpha value to a target in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading.
    /// </summary>
    public static void FadeAlphaHierarchy(GameObject obj, float alpha, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        int key = LowestAvailableKey();
        hierarchyLists.Add(key, new List<Transform>());
        FadeAlphaRecursion(key, obj, alpha, duration, colorProperty, endFadeFunction);
    }

    private static void FadeAlphaRecursion(int key, GameObject obj, float alpha, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        Color targetColor = ColorManager.GetObjectColor(obj, colorProperty);
        targetColor.a = alpha;

        // Check if object has renderer and the renderer has the specified color property.
        // If not, then the hierarchy doesnt need to wait for it fade, since it won't fade.
        if (endFadeFunction != null)
        {
            if (obj.GetComponent<Renderer>() != null && obj.activeInHierarchy)
                if (obj.GetComponent<Renderer>().material.HasProperty(colorProperty))
                    ((List<Transform>)hierarchyLists[key]).Add(obj.transform);

            FadeColor(obj, targetColor, duration, colorProperty, () => ObjectsFadeEndSync(endFadeFunction, key, obj.transform));
        }
        else
            FadeColor(obj, targetColor, duration, colorProperty);

        foreach (Transform child in obj.transform)
            FadeAlphaRecursion(key, child.gameObject, alpha, duration, colorProperty, endFadeFunction);
    }

    /// <summary>
    /// Fades the color of a single object to a target color in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading.
    /// </summary>
    /// <returns></returns>
    public static int FadeColor(GameObject obj, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (obj.activeInHierarchy && renderer != null)
        {
            if (renderer.material.HasProperty(colorProperty))
            {
                if (fadeRoutines.ContainsKey(obj)) 
                {
                    Coroutine fadeRoutine = fadeRoutines[obj];
                    RoutineHelper.mono.StopCoroutine(fadeRoutine);
                    if (endFadeFunctions.ContainsKey(fadeRoutine))
                    {
                        endFadeFunctions[fadeRoutine].Invoke();
                        endFadeFunctions.Remove(fadeRoutine);
                    }
                    fadeRoutines.Remove(obj);
                }

                RoutineHelper.mono.StartCoroutine(ObjectFader.FadeObject(obj, targetColor, duration, colorProperty, endFadeFunction));
                return 1;
            }
        }

        return 0;
    }

    public static void FadeColorArray(GameObject[] objects, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        int key = LowestAvailableKey();
        hierarchyLists.Add(key, new List<Transform>());

        foreach (GameObject obj in objects)
        {
            if (obj.GetComponent<Renderer>() != null && obj.activeInHierarchy)
                if (obj.GetComponent<Renderer>().material.HasProperty(colorProperty))
                    ((List<Transform>)hierarchyLists[key]).Add(obj.transform);

            FadeColor(obj, targetColor, duration, colorProperty, () => ObjectsFadeEndSync(endFadeFunction, key, obj.transform));
            Debug.Log("Key: " + key + "Fading obj: " + obj.name);
        }
    }

    /// <summary>
    /// Fades the color of a list of objects to a target color in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading all objects.
    /// </summary>
    public static void FadeColorList(List<GameObject> objects, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        int key = LowestAvailableKey();
        hierarchyLists.Add(key, new List<Transform>());

        foreach (GameObject obj in objects)
        {
            if (obj.GetComponent<Renderer>() != null && obj.activeInHierarchy)
                if (obj.GetComponent<Renderer>().material.HasProperty(colorProperty))
                    ((List<Transform>)hierarchyLists[key]).Add(obj.transform);

            FadeColor(obj, targetColor, duration, colorProperty, () => ObjectsFadeEndSync(endFadeFunction, key, obj.transform));
        }
    }

    /// <summary>
    /// Fades the color of an hierarchy of objects to a target color in the specified duration. 
    /// If endFadeFunction is defined, evokes the function after done fading all objects.
    /// </summary>
    public static void FadeColorHierarchy(GameObject obj, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        int key = LowestAvailableKey();

        hierarchyLists.Add(key, new List<Transform>());
        FadeColorRecursion(key, obj, targetColor, duration, colorProperty, endFadeFunction);
    }

    private static void FadeColorRecursion(int key, GameObject obj, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade endFadeFunction = null)
    {
        if (endFadeFunction != null)
        {
            if (obj.GetComponent<Renderer>() != null && obj.activeInHierarchy)
                if (obj.GetComponent<Renderer>().material.HasProperty(colorProperty))
                    ((List<Transform>)hierarchyLists[key]).Add(obj.transform);

            FadeColor(obj, targetColor, duration, colorProperty, () => ObjectsFadeEndSync(endFadeFunction, key, obj.transform));
        }
        else
            FadeColor(obj, targetColor, duration, colorProperty);

        foreach (Transform child in obj.transform)
            FadeColorRecursion(key, child.gameObject, targetColor, duration, colorProperty, endFadeFunction);
    }

    /// <summary>
    /// Function for syncronizing End Fade Functions for every object in an hierarchy. 
    /// End Fade Function is only executed when every object in the hierarchy has successfully faded.
    /// </summary>
    /// <param name="args"></param>
    private static void ObjectsFadeEndSync(OnObjectEndFade hierarchyEndFunc, int key, Transform endedObj)
    {
        if (hierarchyLists.ContainsKey(key))
        {
            List<Transform> remainingObjects = (List<Transform>)hierarchyLists[key];
            remainingObjects.Remove(endedObj);

            if (remainingObjects.Count == 0)
            {
                hierarchyLists.Remove(key);
                hierarchyEndFunc();
            }
        }
    }

    private static int LowestAvailableKey()
    {
        int lowest = 0;
        while (hierarchyLists.ContainsKey(lowest)) { lowest++; }
        return lowest;
    }

    #region Fading Coroutines
    private static IEnumerator FadeObjectRoutine(GameObject target, Color targetColor, float duration, string colorProperty = "_Color")
    {
        Color objColor = ColorManager.GetObjectColor(target, colorProperty);
        Color initialColor = objColor;
        float interpStep = 0f;

        while (interpStep <= 0.95f)
        {
            yield return null;
            interpStep += Time.deltaTime / duration;
            objColor = Color.Lerp(initialColor, targetColor, interpStep);
            ColorManager.SetObjectColor(target, objColor, colorProperty);
        }

        ColorManager.SetObjectColor(target, targetColor, colorProperty);

        fadeRoutines.Remove(target);
    }

    private static IEnumerator FadeObject(GameObject target, Color targetColor, float duration, string colorProperty = "_Color", OnObjectEndFade onObjectEndFade = null)
    {
        Coroutine fadeRoutine = RoutineHelper.mono.StartCoroutine(ObjectFader.FadeObjectRoutine(target, targetColor, duration, colorProperty));
        fadeRoutines.Add(target, fadeRoutine);
        if (onObjectEndFade != null)
            endFadeFunctions.Add(fadeRoutine, onObjectEndFade);

        yield return fadeRoutine;
        
        if (onObjectEndFade != null)
            onObjectEndFade();
    }
    #endregion


}
