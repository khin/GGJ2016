﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorManager : MonoBehaviour {

    static Hashtable originalColors = new Hashtable();

    public static Color GetOriginalColor(GameObject obj, string colorProperty = "_Color")
    {
        if (originalColors.ContainsKey(obj))
            return (Color)originalColors[obj];
        else
            return GetObjectColor(obj, colorProperty);
    }

    public static void RevertObjectColor(GameObject obj, string colorProperty = "_Color")
    {
        if(originalColors.ContainsKey(obj))
            SetObjectColor(obj, (Color) originalColors[obj], colorProperty);
    }

    public static Color GetObjectColor(GameObject obj, string colorProperty = "_Color")
    {
        if (!obj.GetComponent<Renderer>())
            return Color.white;

        Material objMaterial = obj.GetComponent<Renderer>().material;
        TextMesh objTextMesh = obj.GetComponent<TextMesh>();

        if (objTextMesh != null) 
            return objTextMesh.color;
        else if (objMaterial.HasProperty(colorProperty)) 
            return objMaterial.GetColor(colorProperty);
        else 
            return Color.white;
    }

    public static void SetObjectColor(GameObject obj, Color color, string colorProperty = "_Color")
    {
        if (!obj.GetComponent<Renderer>())
            return;

        if (!originalColors.ContainsKey(obj))
            originalColors.Add(obj, GetObjectColor(obj, colorProperty));

        Material objMaterial = obj.GetComponent<Renderer>().material;
        TextMesh objTextMesh = obj.GetComponent<TextMesh>();

        if (objMaterial.HasProperty(colorProperty)) objMaterial.SetColor(colorProperty, color);
        if (objTextMesh != null) objTextMesh.color = color;
    }

    public static void SetObjectAlpha(GameObject obj, float alpha, string colorProperty = "_Color")
    {
        Color objColor = GetObjectColor(obj, colorProperty);
        objColor.a = alpha;
        SetObjectColor(obj, objColor, colorProperty);
    }

    public static void SetHierarchyColor(GameObject root, Color color, string colorProperty = "_Color")
    {
        SetObjectColor(root, color, colorProperty);
        foreach (Transform child in root.transform)
            SetHierarchyColor(child.gameObject, color, colorProperty);
    }

    public static void SetHierarchyAlpha(GameObject root, float alpha, string colorProperty = "_Color")
    {
        SetObjectAlpha(root, alpha, colorProperty);
        foreach(Transform child in root.transform)
            SetHierarchyAlpha(child.gameObject, alpha, colorProperty);
    }
}
