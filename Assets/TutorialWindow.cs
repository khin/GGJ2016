﻿using UnityEngine;
using System.Collections;

public class TutorialWindow : MonoBehaviour {

	public void StartTutorial () {
        gameObject.SetActive(false);
        ScreenOverlay.SetAlpha(0.5f);
	}

    public void SkipTutorial ()
    {
        gameObject.SetActive(false);
    }
}
