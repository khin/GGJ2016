﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour, IObserver {

    private RectTransform rect {
        get { return GetComponent<RectTransform>(); }
    }

    [SerializeField]
    private Player player;
    private float startSize;
    private float maxHealth;

	// Use this for initialization
	void Start () {
        startSize = rect.sizeDelta.x;
        if (player != null)
        {
            this.RegisterTo(player.GetComponent<Health>());
            maxHealth = player.GetComponent<Health>().HPvalue;
        }
	}
	
	public void OnNotification(object sender, string message, params object[] args)
    {
        
        if(message.Equals(Messages.PlayerHealthChanged))
        {
            float currentHP = (float)args[0];
            rect.sizeDelta = new Vector2(currentHP / maxHealth * startSize, rect.sizeDelta.y);
        }
    }
}
