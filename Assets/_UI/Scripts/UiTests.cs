﻿using UnityEngine;
using System.Collections;

public class UiTests : MonoBehaviour {

    public GameObject pauseCanvas;
    public GameObject resultsCanvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        // Test: Show pause UI
        if (Input.GetKey(KeyCode.P))
        {
            if(pauseCanvas && !pauseCanvas.active)
            {
                pauseCanvas.SetActive(true);
            }
        }

        // Test: Show match results UI
        if (Input.GetKey(KeyCode.R))
        {
            if (resultsCanvas && !resultsCanvas.active)
            {
                resultsCanvas.SetActive(true);
            }
        }

    }
}
