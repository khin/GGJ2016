﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenOverlay : MonoBehaviour {

    private Image image
    {
        get { return GetComponent<Image>(); }
    }
    public static ScreenOverlay instance;

    void Start()
    {
        ScreenOverlay.instance = this;
    }

	public static void SetColor(Color color)
    {
        instance.image.color = color;
    }

    public static void SetAlpha(float alpha)
    {
        Color newColor = new Color(instance.image.color.r, instance.image.color.g, instance.image.color.b, alpha);
        instance.image.color = newColor;
    }
}
