﻿using UnityEngine;
using System.Collections;

public class ElementAura : MonoBehaviour {

	public GameObject fireAura;
	public GameObject waterAura;
	public GameObject earthAura;
	public GameObject windAura;
	
	public void SetAura(ElementType type) {
		fireAura.SetActive(false);
		waterAura.SetActive(false);
		earthAura.SetActive(false);
		windAura.SetActive(false);

		if (type == ElementType.Fire)
			fireAura.SetActive(true);
		else if (type == ElementType.Water)
			waterAura.SetActive(true);
		else if (type == ElementType.Earth)
			earthAura.SetActive(true);
		else if (type == ElementType.Wind)
			windAura.SetActive(true);
	}
}
