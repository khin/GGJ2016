﻿using UnityEngine;
using System.Collections;

public class SmokeCluster : MonoBehaviour {

	public GameObject[] smoke;

	IEnumerator Start () {


		smoke[0].SetActive(true);
		yield return new WaitForSeconds(0.3f);
		smoke[1].SetActive(true);
		yield return new WaitForSeconds(0.3f);
		smoke[2].SetActive(true);
	}
	
	
}
