﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	float elapsedTime = 0;
	public float maxDuration;
	public GameObject gds;
	Image logo;
	Color cImg;

	// Use this for initialization
	void Start () {
		logo = gds.GetComponent<Image> ();
		cImg = logo.color;
	}
	
	// Update is called once per frame
	void Update () {
		elapsedTime += Time.deltaTime;
		if (elapsedTime > maxDuration) {
			SceneManager.LoadScene ("TitleScreen");
		} else if (elapsedTime < maxDuration / 3) {
			cImg.a += 1 * Time.deltaTime;
		} else if (elapsedTime > 2*(maxDuration / 3)) {
			cImg.a -= 2 * Time.deltaTime;
		}
		logo.color = cImg;
	}
}
