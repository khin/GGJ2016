﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ResultCanvas : MonoBehaviour, IObserver {

	public GameObject[] resultScreenObjects;
	public GameObject player1WinMessage;
	public GameObject player2WinMessage;
	public GameObject BGM;

	// Use this for initialization
	IEnumerator Start () {
		while (!Player.refs[0])
			yield return null;

		this.RegisterTo(Player.refs[0].GetComponent<Health>());
		this.RegisterTo(Player.refs[1].GetComponent<Health>());
	}
	
	public void OnNotification(object sender, string message, params object[] args) {
		if(message == Messages.PlayerDeath) {
			Health health = sender as Health;
			GameObject gameObj = health.gameObject;
			ActivateResultScreen();
			if (LayerMask.LayerToName(gameObj.layer) == "Player1") {
				player2WinMessage.SetActive(true);
			}
			else {
				player1WinMessage.SetActive(true);
			}
			BGM.SetActive(false);
		}
	}

	private void ActivateResultScreen() {
		Player.refs[0].GetComponent<InputHandler>().enabled = false;
		Player.refs[1].GetComponent<InputHandler>().enabled = false;
		foreach (GameObject obj in resultScreenObjects)
			obj.SetActive(true);
	}

	public void ReloadGame() {
		SceneManager.LoadScene("Game");
	}

	public void LoadTitle() {
		SceneManager.LoadScene("TitleScreen");
	}
}
