﻿using UnityEngine;
using System.Collections;

public class Totem : MonoBehaviour, IObserver {

	public GameObject[] smokeClusters;
	public Player player;

	private float startHealth;

	// Use this for initialization
	void Start () {
		this.RegisterTo(player.GetComponent<Health>());
		startHealth = player.GetComponent<Health>().HPvalue;
	}
	
	public void OnNotification(object sender, string message, params object[] args) {
		if(message.Equals(Messages.PlayerHealthChanged)) {
			float currentValue = (float)args[0];
			float percentage = currentValue / startHealth;

			if (percentage < 0.75f)
				smokeClusters[0].SetActive(true);
			if (percentage < 0.5f)
				smokeClusters[1].SetActive(true);
			if (percentage < 0.25f)
				smokeClusters[2].SetActive(true);
			if (percentage < 0.1f)
				smokeClusters[3].SetActive(true);
		}
	}
}
